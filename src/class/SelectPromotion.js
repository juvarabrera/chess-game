class SelectPromotion {
    constructor(chess, position, piece) {
        this.div = document.createElement("div");
        this.div.classList.add("select-promotion");
        if(Chess.COLUMNS.indexOf(position.split("")[0]) > 4)
            this.div.classList.add("right")
        if(piece.team === Chess.TEAM.BLACK)
            this.div.classList.add("top")
        
        this.div.innerHTML += `<div class="chess-piece team-${piece.team}" data-official="Queen"><i class="mdi mdi-chess-queen"></i></div>`;
        this.div.innerHTML += `<div class="chess-piece team-${piece.team}" data-official="Rook"><i class="mdi mdi-chess-rook"></i></div>`;
        this.div.innerHTML += `<div class="chess-piece team-${piece.team}" data-official="Knight"><i class="mdi mdi-chess-knight"></i></div>`;
        this.div.innerHTML += `<div class="chess-piece team-${piece.team}" data-official="Bishop"><i class="mdi mdi-chess-bishop"></i></div>`;
        const cellDiv = document.querySelector(`.cell[data-position="${position}"]`)
        cellDiv.insertAdjacentElement("beforeend", this.div);
        const obj = this;
        document.querySelectorAll(".select-promotion .chess-piece").forEach(element => {
            element.addEventListener("click", (event) => {
                chess.promote(piece, element.getAttribute("data-official"));
                obj.div.remove();
            })
        })
    }
}