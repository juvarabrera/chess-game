class Chess {
    static TEAM = {
        "WHITE": 0,
        "BLACK": 1
    }
    static COLUMNS = "abcdefgh"
    constructor() {
        this.boardDiv = document.querySelector(".board");
        this.pieceDragged = null;
        this.board = [];
        for(let i = 0; i < 8; i++) {
            this.board.push([null,null,null,null,null,null,null,null])
        }
    }
    dragPiece(event) {
        this.handleCellClick(event);
    }
    dropPiece(event) {
        event.preventDefault();
        this.handleCellClick(event);
    }
    dragOver(event) {
        event.preventDefault();
    }
    getPieceInBoard(position) {
        position = position.split("");
        return this.board[8 - position[1]][Chess.COLUMNS.indexOf(position[0])];
    }
    assignToBoard(piece, position) {
        position = position.split("");
        this.board[8 - position[1]][Chess.COLUMNS.indexOf(position[0])] = piece;

    }
    highlightPossibleMoves(cells) {
        for(let i = 0; i < cells.length; i++) {
            document.querySelector(`.cell[data-position="${cells[i]}"]`).querySelector(".possibleMove").classList.add("show");
        }
    }
    removePossibleMoves() {
        const cells = document.querySelectorAll(".possibleMove");
        for(let i = 0; i < cells.length; i++) {
            cells[i].classList.remove("show");
        }
    }
    resetBoard() {
        this.boardDiv.innerHTML = "";
        for(let row = 8; row >= 1; row--) {
            let rowDiv = document.createElement("div")
            rowDiv.classList.add("row")
            for(let column = 0; column < Chess.COLUMNS.length; column++) {
                let columnDiv = document.createElement("div");
                columnDiv.classList.add("cell");
                columnDiv.setAttribute("data-position", Chess.COLUMNS[column] + row);
                
                const positionLabel = document.createElement("span");
                positionLabel.textContent = Chess.COLUMNS[column] + "" + row;
                columnDiv.insertAdjacentElement("beforeend", positionLabel);

                const possibleMove = document.createElement("div");
                possibleMove.classList.add("possibleMove");
                columnDiv.insertAdjacentElement("beforeend", possibleMove);

                columnDiv.addEventListener("click", event => this.handleCellClick(event))
                columnDiv.addEventListener("dragover", event => this.dragOver(event));
                columnDiv.addEventListener("drop", event => this.dropPiece(event));
                rowDiv.insertAdjacentElement("beforeend", columnDiv);
            }
            this.boardDiv.insertAdjacentElement("beforeend", rowDiv);
        }

        this.spawnOfficials(Chess.TEAM.BLACK, 8);
        this.spawnOfficials(Chess.TEAM.WHITE, 1);
        this.spawnPawns(Chess.TEAM.BLACK, 7);
        this.spawnPawns(Chess.TEAM.WHITE, 2);

        this.selectedCell = null;
        this.moves = 0;
        
        this.statusBox = document.createElement("div");
        this.statusBox.classList.add("status-box");
        this.boardDiv.insertAdjacentElement("beforeend", this.statusBox);
        this.changeTeamToMove(Chess.TEAM.WHITE);
        this.changeStatus((this.teamToMove === Chess.TEAM.WHITE ? "White" : "Black") + " to move.")
    }

    changeTeamToMove(team = null) {
        if(team !== null) {
            this.teamToMove = team;
        } else {
            this.teamToMove = this.teamToMove === Chess.TEAM.WHITE ? Chess.TEAM.BLACK : Chess.TEAM.WHITE;
        }
        this.changeStatus((this.teamToMove === Chess.TEAM.WHITE ? "White" : "Black") + " to move.")
    }

    spawnPawns(team, row) {
        for(let column = 0; column < Chess.COLUMNS.length; column++) { 
            const pawn = new Pawn(team);
            this.spawn(pawn, Chess.COLUMNS[column] + row);
        }
    }
    spawnOfficials(team, row) {
        const officials = [
            new Rook(team),
            new Knight(team),
            new Bishop(team),
            new Queen(team),
            new King(team),
            new Bishop(team),
            new Knight(team),
            new Rook(team)
        ];
        for(let index in officials) {
            this.spawn(officials[index], Chess.COLUMNS[index] + row)
        }
    }
    handleCellClick(event) {
        let cell = event.target;
        if(cell.nodeName !== "DIV" || !cell.classList.contains("cell"))
            cell = cell.offsetParent;
        const position = cell.getAttribute("data-position");
        if(this.selectedCell === null) {
            const piece = this.getPieceInBoard(position);
            if(piece !== null) {
                if(piece.team === this.teamToMove) {
                    const availableMoves = piece.getAvailableMoves(this);
                    this.highlightPossibleMoves(availableMoves);
                    this.selectedCell = position;
                }
            }
        } else {
            const tempCell = this.getPieceInBoard(position);
            const piece = this.getPieceInBoard(this.selectedCell);
            const availableMoves = piece.getAvailableMoves(this);
            
            if(availableMoves.indexOf(position) !== -1) {
                if(this.move(piece, position)) {
                    if(tempCell !== null)
                        this.remove(tempCell)
                } else {
                    this.selectedCell = null;
                    this.removePossibleMoves();
                }
            } else {
                this.selectedCell = null;
                this.removePossibleMoves();
            }
        }
    }
    spawn(piece, position) {
        piece.place(position);
        this.assignToBoard(piece, position);
        piece.element.addEventListener("dragstart", event => this.dragPiece(event));
    }
    remove(piece) {
        // this.assignToBoard(null, piece.position);
        piece.remove();
    }
    getOpposingTeam(team) {
        return team === Chess.TEAM.WHITE ? Chess.TEAM.BLACK : Chess.TEAM.WHITE;
    }
    move(piece, newPosition) {
        const tempPiece = this.getPieceInBoard(newPosition);
        this.assignToBoard(null, this.selectedCell);
        this.assignToBoard(piece, newPosition);
        
        if(this.isCheck()) {
            this.changeStatus("Invalid move. The King is still check...", "warning")
            this.assignToBoard(tempPiece, newPosition);
            this.assignToBoard(piece, this.selectedCell);
            return false;
        }
        
        this.moves++;
        
        if(piece.constructor.name === "King")
            piece.cast(newPosition, this);
        else if(piece.constructor.name === "Pawn") {
            const currentRow = parseInt(this.selectedCell.split("")[1]);
            const newRow = parseInt(newPosition.split("")[1]);
            if(Math.abs(currentRow - newRow) === 2) piece.moveTwo = true;
            if(piece.position.split("")[0] !== newPosition.split("")[0] && tempPiece === null) {
                const enemyPawnPosition = newPosition.split("")[0] + (parseInt(newPosition.split("")[1]) + (this.team === Chess.TEAM.WHITE ? 1 : -1));
                this.remove(this.getPieceInBoard(enemyPawnPosition))
            }
        }
        piece.move(newPosition, this.moves);
        this.selectedCell = null;
        this.removePossibleMoves();

        if(piece.constructor.name === "Pawn") {
            if(piece.forPromotion(newPosition)) {
                new SelectPromotion(this, newPosition, piece);
            }
        }
        
        this.changeTeamToMove();
        if(this.isCheck()) {
            this.checkKing();
            if(this.isCheckmate()) {
                this.checkmate();
            }
        } else if(this.isStalemate()) {
            this.stalemate();
        }
        return true;
    }
    getKing(team) {
        for(let y = 0; y < this.board.length; y++) {
            for(let x = 0; x < this.board[y].length; x++) {
                const piece = this.board[y][x];
                if(piece === null) continue;
                if(piece.team !== team) continue;
                if(piece.constructor.name === "King")
                    return piece;
            }
        }
        return null;
    }
    getTeamPieces(team) {
        let pieces = [];
        for(let y = 0; y < this.board.length; y++) {
            for(let x = 0; x < this.board[y].length; x++) {
                const piece = this.board[y][x];
                if(piece === null) continue;
                if(piece.team !== team) continue;
                pieces.push(piece);
            }
        }
        return pieces;
    }
    isStalemate() {
        const enemyPieces = this.getTeamPieces(this.getOpposingTeam(this.teamToMove));
        let enemyPiecesMoves = [];
        for(let i in enemyPieces) {
            const enemyPiece = enemyPieces[i];
            enemyPiecesMoves.push(...enemyPiece.getAvailableMoves(this));
            enemyPiecesMoves.push(...enemyPiece.getGuardedPiece(this));
        }
        const king = this.getKing(this.teamToMove);
        const kingMoves = king.getAvailableMoves(this);
        for(let i in kingMoves) {
            let isGuardedByEnemy = (enemyPiecesMoves.indexOf(kingMoves[i]) !== -1);
            if(!isGuardedByEnemy) return false;
        }

        const teamPieces = this.getTeamPieces(this.teamToMove);
        for(let i in teamPieces) {
            const teamPiece = teamPieces[i];
            if(teamPiece.constructor.name === "King") continue;
            const teamPieceMoves = teamPiece.getAvailableMoves(this);
            if(teamPieceMoves.length !== 0) return false;
        }
        return true;
    }
    stalemate() {
        this.teamToMove = null;
        this.changeStatus("Stalemate. No one wins.", "end")
    }
    isCheckmate() {
        const king = this.getKing(this.teamToMove);
        const kingMoves = king.getAvailableMoves(this);
        const teamPieces = this.getTeamPieces(this.teamToMove);
        const enemyPieces = this.getTeamPieces(this.getOpposingTeam(this.teamToMove));
        let enemyPiecesMoves = [];
        for(let i in enemyPieces) {
            const enemyPiece = enemyPieces[i];
            enemyPiecesMoves.push(...enemyPiece.getAvailableMoves(this));
            enemyPiecesMoves.push(...enemyPiece.getGuardedPiece(this));
        }
        for(let i in kingMoves) {
            let isGuardedByEnemy = (enemyPiecesMoves.indexOf(kingMoves[i]) !== -1);
            if(!isGuardedByEnemy) return false;
        }
        for(let i in teamPieces) {
            const piece = teamPieces[i];
            const currentPosition = piece.position;
            const pieceMoves = piece.getAvailableMoves(this);
            for(let j in pieceMoves) {
                const pieceMove = pieceMoves[j];
                const tempPiece = this.getPieceInBoard(pieceMove);
                this.assignToBoard(null, currentPosition);
                this.assignToBoard(piece, pieceMove);
                const isCheck = this.isCheck();
                this.assignToBoard(tempPiece, pieceMove);
                this.assignToBoard(piece, currentPosition);
                if(!isCheck) return false;
            }
        }
        return true;
    }
    checkmate() {
        this.teamToMove = null;
        const enemyTeam = this.getOpposingTeam(this.teamToMove);
        this.changeStatus("Checkmate. " + (enemyTeam === Chess.TEAM.WHITE ? "White": "Black") + " wins.", "end")
    }
    changeStatus(message, statusType = null) {
        this.statusBox.classList.remove("end", "warning");
        this.statusBox.innerHTML = message;
        if(statusType !== null)
            this.statusBox.classList.add(statusType)
    }
    checkKing() { 
        this.statusBox.innerHTML = "Check. " + this.statusBox.innerHTML;
        this.statusBox.classList.add("warning")
    }
    isCheck(board = null) {
        if(board === null) board = this.board;
        const enemyTeam = this.getOpposingTeam(this.teamToMove);
        for(let y = 0; y < board.length; y++) {
            for(let x = 0; x < board[y].length; x++) {
                const piece = board[y][x];
                if(piece === null) continue;
                if(piece.team !== enemyTeam) continue;
                const availableMoves = piece.getAvailableMoves(this);
                for(let i in availableMoves) {
                    const tempPiece = this.getPieceInBoard(availableMoves[i]);
                    if(tempPiece === null) continue;
                    if(tempPiece.constructor.name !== "King") continue;
                    return true;
                }
            }
        }
        return false;
    }
    promote(piece, official) {
        piece.promote(this, official)
    }
}