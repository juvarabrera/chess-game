class Rook extends Piece {
    constructor(team) {
        super(team);
        this.moves = [
            Piece.DIRECTION.UP,
            Piece.DIRECTION.DOWN,
            Piece.DIRECTION.LEFT,
            Piece.DIRECTION.RIGHT
        ];
    }
    getGuardedPiece(chess) {
        return this.getCells(chess, this.moves, null, true);
    }
    getAvailableMoves(chess) {
        return this.getCells(chess, this.moves);
    }
}