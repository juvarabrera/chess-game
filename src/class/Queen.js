class Queen extends Piece {
    constructor(team) {
        super(team);
        this.moves = [
            Piece.DIRECTION.UP,
            Piece.DIRECTION.DOWN,
            Piece.DIRECTION.LEFT,
            Piece.DIRECTION.RIGHT,
            Piece.DIRECTION.UP_LEFT,
            Piece.DIRECTION.UP_RIGHT,
            Piece.DIRECTION.DOWN_LEFT,
            Piece.DIRECTION.DOWN_RIGHT
        ];
    }
    getGuardedPiece(chess) {
        return this.getCells(chess, this.moves, null, true);
    }
    getAvailableMoves(chess) {
        return this.getCells(chess, this.moves);
    }
}