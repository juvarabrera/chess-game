class Pawn extends Piece {
    constructor(team) {
        super(team);
        this.moveTwo = false;
    }
    getGuardedPiece(chess) {
        return this.getCells(chess, this.team === Chess.TEAM.WHITE ? 
            [Piece.DIRECTION.UP_LEFT, Piece.DIRECTION.UP_RIGHT] : [Piece.DIRECTION.DOWN_LEFT, Piece.DIRECTION.DOWN_RIGHT]
        , 1, true);
    }
    getAvailableMoves(chess) {
        let cells = []
        let cellsForward = this.getCells(chess, [
            this.team === Chess.TEAM.WHITE ? Piece.DIRECTION.UP : Piece.DIRECTION.DOWN,
        ], 2, false);
        if(this.hasMoved && cellsForward.length > 1) cellsForward.pop()
        for(let i in cellsForward) {
            if(chess.getPieceInBoard(cellsForward[i]) === null) 
                cells.push(cellsForward[i])
        }
        let cellsDiagonal = this.getCells(chess, this.team === Chess.TEAM.WHITE ? 
            [Piece.DIRECTION.UP_LEFT, Piece.DIRECTION.UP_RIGHT] : [Piece.DIRECTION.DOWN_LEFT, Piece.DIRECTION.DOWN_RIGHT]
        , 1);
        for(let i in cellsDiagonal) {
            if(chess.getPieceInBoard(cellsDiagonal[i]) !== null) 
                cells.push(cellsDiagonal[i])
        }
        if((this.team === Chess.TEAM.WHITE && this.position.split("")[1] === "5") || this.team === Chess.TEAM.BLACK && this.position.split("")[1] === "4") {
            const besideCells = this.getCells(chess, [
                Piece.DIRECTION.LEFT, Piece.DIRECTION.RIGHT,
            ], 1, false);
            for(let i in besideCells) {
                const besideCell = chess.getPieceInBoard(besideCells[i]);
                if(besideCell === null) continue;
                if(besideCell.constructor.name !== "Pawn") continue;
                if(!besideCell.moveTwo) continue;
                if(this.lastMove > besideCell.lastMove) continue;
                cells.push(besideCell.position.split("")[0] + (parseInt(besideCell.position.split("")[1]) + (this.team === Chess.TEAM.WHITE ? 1 : -1)));
            }
        }
        return cells;
    }
    forPromotion(position) {
        position = position.split("")
        if((this.team === Chess.TEAM.WHITE && parseInt(position[1]) === 8) || 
            (this.team === Chess.TEAM.BLACK && parseInt(position[1]) === 1))
            return true;
        return false;
    }
    promote(chess, official) {
        this.remove();
        if(official === "Queen")
            chess.spawn(new Queen(this.team), this.position);
        if(official === "Rook")
            chess.spawn(new Rook(this.team), this.position);
        if(official === "Knight")
            chess.spawn(new Knight(this.team), this.position);
        if(official === "Bishop")
            chess.spawn(new Bishop(this.team), this.position);
    }

}