class King extends Piece {
    constructor(team) {
        super(team);
        this.moves = [
            Piece.DIRECTION.UP,
            Piece.DIRECTION.DOWN,
            Piece.DIRECTION.LEFT,
            Piece.DIRECTION.RIGHT,
            Piece.DIRECTION.UP_LEFT,
            Piece.DIRECTION.UP_RIGHT,
            Piece.DIRECTION.DOWN_LEFT,
            Piece.DIRECTION.DOWN_RIGHT
        ];
    }
    getAvailableMoves(chess) {
        let cells = this.getCells(chess, this.moves, 1);
        cells = cells.concat(this.canCast(chess));
        return cells;
    }
    getGuardedPiece(chess) {
        return this.getCells(chess, this.moves, 1, null);
    }
    canCast(chess) {
        if(this.hasMoved) return [];
        let canCastCells = [];
        let cells = this.getCells(chess, [
            Piece.DIRECTION.LEFT,
            Piece.DIRECTION.RIGHT
        ], null, true);
        for(let i in cells) {
            const piece = chess.getPieceInBoard(cells[i]);
            if(piece === null) continue;
            if(piece.constructor.name !== "Rook") continue;
            if(piece.hasMoved) continue;
            const position = cells[i].split("");
            if(position[0] === "h")
                canCastCells.push("g" + position[1]);
            else if(position[0] === "a")
                canCastCells.push("c" + position[1]);
        }
        return canCastCells;
    }
    cast(position, chess) {
        if(this.hasMoved) return false;
        position = position.split("");
        let piece = null;
        let newPosition = null;
        if(position[0] === "c") {
            piece = chess.getPieceInBoard("a" + position[1]);
            newPosition = "d" + position[1];
        } else if(position[0] === "g") {
            piece = chess.getPieceInBoard("h" + position[1]);
            newPosition = "f" + position[1];
        } else {
            return false;
        }
        if(piece === null) return false;
        if(piece.constructor.name !== "Rook") return false;
        if(piece.hasMoved) return false;
        chess.assignToBoard(null, position.join(""));
        chess.assignToBoard(piece, newPosition);
        piece.move(newPosition, chess.moves);
        chess.selectedCell = "e" + position[1];
    }
}