class Piece {
    static DIRECTION = {
        UP: 0,
        DOWN: 1,
        LEFT: 2,
        RIGHT: 3,
        UP_LEFT: 4,
        UP_RIGHT: 5,
        DOWN_LEFT: 6,
        DOWN_RIGHT: 7,
        L: 8
    }
    constructor(team) {
        this.team = team;
        this.hasMoved = false;
        this.element = document.createElement("div");
        this.element.classList.add("chess-piece", "team-" + this.team, "chess-piece-" + this.constructor.name.toLowerCase());
        const icon = document.createElement("i");
        icon.draggable = true;
        icon.classList.add("mdi", "mdi-chess-" + this.constructor.name.toLowerCase());
        this.element.insertAdjacentElement("beforeend", icon);
        this.lastMove = null;
    }
    place(position) {
        this.position = position;
        const cellDiv = document.querySelector(`.cell[data-position="${position}"]`)
        cellDiv.insertAdjacentElement("beforeend", this.element)
    }
    move(newPosition, lastMove) {
        this.position = newPosition;
        const cellDiv = document.querySelector(`.cell[data-position="${newPosition}"]`)
        cellDiv.append(this.element)
        this.hasMoved = true;
        this.lastMove = lastMove;
    }
    remove() {
        this.element.remove();
    }
    getAvailableMoves() {
        return [];
    }
    getGuardedPiece() {
        return [];
    }
    getCells(chess, directions, max = null, teamOnly = false) {
        const position = this.position.split("");
        let cells = [];
        if(directions.indexOf(Piece.DIRECTION.UP) !== -1) {
            let added = 0;
            for(let i = parseInt(position[1]) + 1; i <= 8; i++) {
                const tempPosition = position[0] + i;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) continue;
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
            }
        }
        if(directions.indexOf(Piece.DIRECTION.DOWN) !== -1) {
            let added = 0;
            for(let i = parseInt(position[1]) - 1; i >= 1; i--) {
                const tempPosition = position[0] + i;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) continue;
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
            }
        }
        if(directions.indexOf(Piece.DIRECTION.LEFT) !== -1) {
            let added = 0;
            for(let i = Chess.COLUMNS.indexOf(position[0]) - 1; i >= 0; i--) {
                const tempPosition = Chess.COLUMNS[i] + position[1];
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) continue;
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
            }
        }
        if(directions.indexOf(Piece.DIRECTION.RIGHT) !== -1) {
            let added = 0;
            for(let i = Chess.COLUMNS.indexOf(position[0]) + 1; i < Chess.COLUMNS.length; i++) {
                const tempPosition = Chess.COLUMNS[i] + position[1];
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) continue;
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
            }
        }
        if(directions.indexOf(Piece.DIRECTION.UP_LEFT) !== -1) {
            let added = 0;
            let move = 1;
            while(move <= 8) {
                const x = Chess.COLUMNS.indexOf(position[0]) - move;
                const y = parseInt(position[1]) + move;
                if(x < 0 || x >= Chess.COLUMNS.length || y < 1 || y > 8) break;
                const tempPosition = Chess.COLUMNS[x] + y;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) { move++; continue; }
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
                move++;
            }
        }
        if(directions.indexOf(Piece.DIRECTION.UP_RIGHT) !== -1) {
            let added = 0;
            let move = 1;
            while(move <= 8) {
                const x = Chess.COLUMNS.indexOf(position[0]) + move;
                const y = parseInt(position[1]) + move;
                if(x < 0 || x >= Chess.COLUMNS.length || y < 1 || y > 8) break;
                const tempPosition = Chess.COLUMNS[x] + y;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) { move++; continue; }
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
                move++;
            }
        }
        if(directions.indexOf(Piece.DIRECTION.DOWN_LEFT) !== -1) {
            let added = 0;
            let move = 1;
            while(move <= 8) {
                const x = Chess.COLUMNS.indexOf(position[0]) - move;
                const y = parseInt(position[1]) - move;
                if(x < 0 || x >= Chess.COLUMNS.length || y < 1 || y > 8) break;
                const tempPosition = Chess.COLUMNS[x] + y;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) { move++; continue; }
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
                move++;
            }
        }
        if(directions.indexOf(Piece.DIRECTION.DOWN_RIGHT) !== -1) {
            let added = 0;
            let move = 1;
            while(move <= 8) {
                const x = Chess.COLUMNS.indexOf(position[0]) + move;
                const y = parseInt(position[1]) - move;
                if(x < 0 || x >= Chess.COLUMNS.length || y < 1 || y > 8) break;
                const tempPosition = Chess.COLUMNS[x] + y;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) { move++; continue; }
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) break;
                if(max === null || (added < max)) {
                    cells.push(tempPosition);
                    if(piece !== null) break;
                    added++;
                }
                move++;
            }
        }
        if(directions.indexOf(Piece.DIRECTION.L) !== -1) {
            let moves = [
                [-1,2],
                [-2,1],
                [-2,-1],
                [-1,-2],
                [1,-2],
                [2,-1],
                [2,1],
                [1,2]
            ]
            for(let i = 0; i < moves.length; i++) {
                const x = Chess.COLUMNS.indexOf(position[0]) + moves[i][0];
                const y = parseInt(position[1]) + moves[i][1];
                if(x < 0 || x >= Chess.COLUMNS.length || y < 1 || y > 8) continue;
                const tempPosition = Chess.COLUMNS[x] + y;
                const piece = chess.getPieceInBoard(tempPosition);
                if(piece === null && teamOnly) continue;
                if(piece !== null) if((piece.team === this.team && !teamOnly) || (piece.team !== this.team && teamOnly)) continue;
                cells.push(tempPosition);
            }
        }

        return cells;
    }
}