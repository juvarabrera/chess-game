class Knight extends Piece {
    constructor(team) {
        super(team);
        this.moves = [
            Piece.DIRECTION.L
        ];
    }
    getGuardedPiece(chess) {
        return this.getCells(chess, this.moves, 1, true);
    }
    getAvailableMoves(chess) {
        return this.getCells(chess, this.moves, 1);
    }
}